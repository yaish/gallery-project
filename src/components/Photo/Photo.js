import React from "react";
import "./Photo.css";

const photo = props => (
  <div className="Photo">
    <img src={props.src} alt="Avatar" />
    <div className="description">
      <p>{props.description}</p>
    </div>
  </div>
);

export default photo;
