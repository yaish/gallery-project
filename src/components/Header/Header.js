import React from "react";
import classes from "./Header.css";

const header = props => (
  <div className="Header">
    <h2>{props.text}</h2>
  </div>
);

export default header;