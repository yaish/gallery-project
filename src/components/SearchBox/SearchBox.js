import React from "react";
import "./SearchBox.css";

class SearchBox extends React.Component {
  constructor(props) {
    super(props);
    this.searchInput = React.createRef();
  }

  handleKeyDown = event => {
    if (event.key === "Enter") {
      this.props.onSubmitSearch(event.target.value);
    }
  };
  render() {
    return (
      <div className="SearchBox">
        <input
          ref={this.searchInput}
          type="text"
          onKeyDown={this.handleKeyDown}
          placeholder={this.props.placeholder}
        />
        <i
          className="SearchIcon fa fa-search"
          onClick={() => this.props.onSubmitSearch(this.searchInput.current.value)}
        />
      </div>
    );
  }
}
export default SearchBox;
