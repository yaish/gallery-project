import React from "react";
import PaginationItem from "./PaginationItem/PaginationItem";
import "./PaginationBar.css";

class PaginationBar extends React.Component {
  render() {
    const paginationItems = this.props.pages.map(page => {
      return (
        <PaginationItem
          order={page.order}
          clicked={() => this.props.clicked(page)}
          isActive={page.order === this.props.selectedPageOrder}
        />
      );
    });
    return (
      <ul className="PaginationBar">
        {paginationItems}
      </ul>
    );
  }
}
export default PaginationBar;
