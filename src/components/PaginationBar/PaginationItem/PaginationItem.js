import React from "react";
import "./PaginationItem.css";

const paginationItem = props => (
  <li
    onClick={props.clicked}
    className={props.isActive ? "active" : null}
  >
    <a onClick={props.clicked}>{props.order}</a>
  </li>
);

export default paginationItem;
