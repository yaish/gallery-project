import React from "react";
import "./App.css";
import Gallery from "./containers/Gallery/Gallery";
import SearchBox from "./components/SearchBox/SearchBox";
import Header from "./components/Header/Header";
import ImagesApiInstance from "./services/ImagesApiService/ImagesApiService";
import PaginationBar from "./components/PaginationBar/PaginationBar";

const images = [
  {
    src: "https://www.w3schools.com/css/img_5terre.jpg",
    description: "Add a description of the image here"
  },
  {
    src: "https://www.w3schools.com/css/img_forest.jpg",
    description: "Add a description of the image here"
  },
  {
    src: "https://www.w3schools.com/css/img_lights.jpg",
    description: "Add a description of the image here"
  },
  {
    src: "https://www.w3schools.com/css/img_5terre.jpg",
    description: "Add a description of the image here"
  },
  {
    src: "https://www.w3schools.com/css/img_mountains.jpg",
    description: "Add a description of the image here"
  },
  {
    src: "https://www.w3schools.com/css/img_5terre.jpg",
    description: "Add a description of the image here"
  }
];

const MAX_AMOUNT_PER_PAGE = 5;
const DEFAULT_PAGE = 1;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [],
      imagesToDisplayByPage: [],
      pages: [],
      selectedPageOrder: DEFAULT_PAGE
    };
  }

  pageChanged = selectedPage => {
    const imagesToDisplayByPage = this.getImagesByPage(
      selectedPage.order,
      this.state.images
    );
    this.setState({
      imagesToDisplayByPage: imagesToDisplayByPage,
      selectedPageOrder: selectedPage.order
    });
  };

  getImagesByPage = (pageOrder, imagesArray) => {
    return imagesArray.slice(
      (pageOrder - 1) * MAX_AMOUNT_PER_PAGE,
      (pageOrder - 1) * MAX_AMOUNT_PER_PAGE + MAX_AMOUNT_PER_PAGE
    );
  };

  getPages = imagesArray => {
    const numberOfPages = Math.ceil(imagesArray.length / MAX_AMOUNT_PER_PAGE);
    return Array.from({ length: numberOfPages }, (_, index) => ({
      order: index + 1
    }));
  };

  onSubmitSearch = async term => {
    const response = await ImagesApiInstance.getImages("/search/photos", {
      params: {
        query: term
      }
    });
    console.log(response);

    const selectedPageOrder = DEFAULT_PAGE;
    const imagesArray = response.data.results;
    const arrayOfPages = this.getPages(imagesArray);
    const imagesToDisplayByPage = this.getImagesByPage(
      selectedPageOrder,
      imagesArray
    );

    this.setState({
      images: imagesArray,
      pages: arrayOfPages,
      imagesToDisplayByPage: imagesToDisplayByPage,
      selectedPageOrder: selectedPageOrder
    });
  };
  render() {
    return (
      <div className="App">
        <div className="App-header-search-container">
          <Header text={"React Gallery Project"} />
          <SearchBox
            onSubmitSearch={this.onSubmitSearch}
            placeholder={"Please search an image"}
          />
        </div>
        <div className="App-gallery-container">
          <PaginationBar
            selectedPageOrder={this.state.selectedPageOrder}
            clicked={this.pageChanged}
            pages={this.state.pages}
          />
          <Gallery images={this.state.imagesToDisplayByPage} />
        </div>

      </div>
    );
  }
}

export default App;
