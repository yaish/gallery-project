import React from "react";
import "./Gallery.css";
import Photo from "../../components/Photo/Photo";

class Gallery extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const imagesList = this.props.images.map(image => {
      return (
        <Photo key={image.id} src={image.urls.regular} description={image.alt_description} />
      );
    });
    return (
      <div className="Gallery">
        {imagesList}
      </div>
    );
  }
}
export default Gallery;
