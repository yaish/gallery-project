import axios from "axios";
const imagesApiIntance = axios.create({
  baseURL: "https://api.unsplash.com/",
  headers: {
    Authorization: "Client-ID 52d5d5565994d57c3160b4296aef1be1bf8985d9265e313f0f9db7eb1145d86d"
  }
});

async function getImages(url, queryParams) {
  return imagesApiIntance.get(url, queryParams);
}

export default { getImages };
